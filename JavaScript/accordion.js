//Accordion init

$( function() {
  $( "#accordion" ).accordion({

    icons: false,
    heightStyle: "content",
    collapsible: true,
    animate: {
      duration: 500,
      easing: 'easeOutQuint',
    },
  });
} );


//Accordion border-bottom destroyer at active st

window.addEventListener ('DOMContentLoaded', function () {

  document.querySelectorAll('#accordion h3').forEach(function(borderContent) { // выбрал каждый h3

    borderContent.addEventListener('click', function() {

      if (borderContent.classList.contains("ui-accordion-header-active")) {

        //console.log(borderContent.querySelector('.questions__item'))

        document.querySelectorAll('.questions__item').forEach(function(gog) {

          if (gog.classList.contains("questions__item_active")) {
            gog.classList.remove("questions__item_active")
          }

        });

        var exam = borderContent.querySelector('.questions__item')
        var path = exam.dataset.border
        document.querySelector(`[data-border="${path}"]`).classList.add('questions__item_active')
      }
      else {
        document.querySelectorAll(`[data-border]`).forEach(function(borderRemove) {
          borderRemove.classList.remove('questions__item_active')
        })

      }

    })
   })
})

//Accordion border-bottom destroyer at active end

