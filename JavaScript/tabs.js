// document.querySelectorAll('.how-we-work__step-link').forEach(function(tabsBtn) {
//   tabsBtn.addEventListener('click', function(animation) {

//     gsap.to(".how-we-work__step-description-left", {duration: 0.2, x:-200, opacity: 0, ease: "power4.out"})
//     gsap.to(".how-we-work__step-description-right", {duration: 0.2, x:200, opacity: 0, ease: "power4.out"})




    window.addEventListener ('DOMContentLoaded', function () {
      document.querySelectorAll('.how-we-work__step-link').forEach(function(tabsBtn) {
        tabsBtn.addEventListener('click', function(event) {

          let tl = gsap.timeline()
          tl.to(".how-we-work__step-description-left", {duration: 0.2, x:-200, opacity: 0, ease: "power4.out"})
            .to(".how-we-work__step-description-right", {duration: 0.2, x:200, opacity: 0, ease: "power4.out"}, "-=0.1")
            .to(".how-we-work__step-description-left", {duration: 0.4, x:0, opacity: 1, ease: "power4.out"})
            .to(".how-we-work__step-description-right", {duration: 0.4, x:0, opacity: 1, ease: "power4.out"}, "-=0.3")

          const path = event.currentTarget.dataset.path

          setTimeout(function() {
            document.querySelectorAll('.how-we-work__step-description-wrapper').forEach(function(tabContent) {
              tabContent.classList.remove('how-we-work__step-description-wrapper_active')
            })
            document.querySelector(`[data-target="${path}"]`).classList.add('how-we-work__step-description-wrapper_active')
          }, 200)






          // initialIn()
          // function initialIn(){
          //   gsap.from(".how-we-work__step-description-left", {duration: 0.8, x:-200, opacity: 0, ease: "power4.out"})
          //   gsap.from(".how-we-work__step-description-right", {duration: 0.8, x:200, opacity: 0, ease: "power4.out"})
          // }

        })
      })
    })











