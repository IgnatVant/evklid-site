window.addEventListener('DOMContentLoaded', function () {

  document.querySelector('#header__menu-burger').addEventListener('click', function () {
    document.querySelector('#header__menu-wrapper').classList.toggle('header__menu-wrapper_active')
  })

  document.querySelector('#header__menu-burger_close').addEventListener('click', function () {
    document.querySelector('#header__menu-wrapper').classList.remove('header__menu-wrapper_active')
  })

  document.querySelector('#main').addEventListener('click', function () {
    document.querySelector('#header__menu-wrapper').classList.remove('header__menu-wrapper_active')
  })

})
