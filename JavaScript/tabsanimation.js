
  
  window.addEventListener ('DOMContentLoaded', function () {
  
  gsap.set(".loader", {opacity: 1, duration: 1.5})

  setTimeout (function() {
      gsap.set(".loader", {opacity: 0})
      // gsap.from(".header", {y:-200, duration: 0.6, ease: "power4.out"})

      let tl = gsap.timeline()
          tl.fromTo(".header__logo", {y:-500, duration: 10}, {y:0})
            .fromTo(".header__nav", {y:-100}, {y:0}, "-=0.2")
            .fromTo(".header__menu-wrapper", {opacity:0}, {opacity:1}, "-=0.2")
            .fromTo(".hero-section__heading", {y:-100, opacity: 0}, {y:0, opacity: 1}, "-=0.3")
            .fromTo(".hero-section__description", {y:-100, opacity: 0}, {y:0, opacity: 1}, "-=0.3")
            .fromTo(".hero-section__btn", {y:-100, opacity: 0}, {y:0, opacity: 1}, "-=0.3")
            .fromTo(".about-us-section", {y:-100, opacity: 0}, {y:0, opacity: 1}, "-=0.9")
            .fromTo(".swiper-pagination", {y:50, opacity: 0, ease: "power4.out"}, {y:0, opacity: 1}, "-=1")
            

      gsap.from(".hero-section__background-image", {y:200, opacity:0, duration: 0.5})

    }, 1500)

  })


  // window.addEventListener ('DOMContentLoaded', function () {
  //   document.querySelector('.loader__ico').style.display = ('inline-block')
  // })
